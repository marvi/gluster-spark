#!/bin/sh

rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm

yum install puppet-agent -y

rm /etc/hosts
touch /etc/hosts

/opt/puppetlabs/bin/puppet apply /vagrant/manifests/site.pp --codedir /vagrant
