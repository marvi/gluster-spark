class common {

  include common::network
  include common::firewall
  include common::packages
  include common::user
  include common::selinux
  include common::ssh

}
