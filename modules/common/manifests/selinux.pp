class common::selinux {

  class { selinux:
    mode => 'permissive',
    type => 'targeted',
    #before => Class['profiles::hadoop_common'],
  }

}
