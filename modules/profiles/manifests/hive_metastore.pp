class profiles::hive_metastore(
  $hdfs_hostname = lookup('hadoop::hdfs_hostname'),
  $domain        = lookup('domain'),
) {

  class{"hive":
    hdfs_hostname       => lookup('hadoop::cluster_name'),
    metastore_hostname  => "${hdfs_hostname}.${domain}",
    server2_hostname    => "${hdfs_hostname}.${domain}",
    zookeeper_hostnames => $quorum_hostnames,
    db                  => 'mysql',
    db_password         => 'hivepassword',
  }

  include ::hive::hdfs
  include ::hive::metastore
  include ::hive::server2
  include ::hive::frontend
  include ::hive::hcatalog

  class { 'mysql::server':
    root_password => 'password'
  }

  Class['hive::metastore::install']
  ->
  mysql::db { 'metastore':
    user     => 'hive',
    password => 'hivepassword',
    host     => 'localhost',
    grant    => ['SELECT', 'UPDATE', 'INSERT', 'DELETE'],
  }
  ->
  exec{ "metastore-import":
    command     => "/usr/bin/mysql metastore < /usr/lib/hive/scripts/metastore/upgrade/mysql/hive-schema-2.1.1.mysql.sql",
    logoutput   => true,
    environment => "HOME=${::root_home}",
    cwd         => '/usr/lib/hive/scripts/metastore/upgrade/mysql',
    refreshonly => true,
  }

  class { 'mysql::bindings':
    java_enable => true,
  }

  Exec['metastore-import'] -> Class['hive::metastore::service']
  Class['mysql::bindings'] -> Class['hive::metastore::config']

}
