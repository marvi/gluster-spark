class profiles::workernode {

  notify { 'Identify host':
    name     => 'Workernode',
    message  => 'Installing workernode...',
  }

  include profiles::hadoop_common
  include hadoop::datanode
  include hadoop::nodemanager

  file { '/usr/bin/hdfs':
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/profiles/hdfs-datanode',
  }

}
